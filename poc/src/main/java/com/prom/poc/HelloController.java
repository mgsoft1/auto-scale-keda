
package com.prom.poc;

import io.micrometer.core.annotation.Timed;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Timed
public class HelloController {

    private Log log = LogFactory.getLog(HelloController.class);

    @Autowired
    ItemService itemService;

    @GetMapping("/log")
    public String log() {
        log.trace("This is a TRACE level message");
        log.debug("This is a DEBUG level message");
        log.info("This is an INFO level message");
        log.warn("This is a WARN level message");
        log.error("This is an ERROR level message");
        return "See the log for details";
    }

    @GetMapping("/exception")
    public String exception() {
        try {
            throw new RuntimeException("hi hi hi");
        } catch (Exception e) {
            log.error(e);
            log.info("-----------------------");
            throw e;
        }

    }

    @GetMapping("/")
    @Timed("api")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @PostMapping("/books")
    @Timed("books.api")
    public String orderBook() {
        return itemService.orderBook();
    }

    @PostMapping("/movies")
    @Timed("movies.api")
    public String orderMovie() {
        return itemService.orderMovie();
    }
}
